import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Pressable, Switch } from 'react-native';
import { useEffect, useState, useSyncExternalStore } from 'react';
import Paho from 'paho-mqtt';

export default function App() {

  //values humidity
  const [humidity, setHumidity] = useState(0);

  //values temperature
  const [temperature, setTemperature] = useState(0);

  //values detection
  const [detection, setDetection] = useState("false");

  //values porte
  const [porte, setPorte] = useState("ferme");

  //values gas
  const [gas, setGas] = useState("false");

  //values fire
  const [fire, setFire] = useState("false");

  // switch values for LED
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => {
    let message = new Paho.Message(isEnabled ? "false" : "true");
    message.destinationName = "house-perfect/led";
    client.send(message);
    setIsEnabled(previousState => !previousState);
  }
  const toggleSwitchExternal = () => {
    setIsEnabled(previousState => !previousState);
  }

  const [client, setClient] = useState(new Paho.Client(
    'broker.hivemq.com',
    Number(8000),
    '/mqtt',
    'myPahoApp'
  ));

  const useRGB = (color) => {
    console.log("rgb changed :", color);
    let message = new Paho.Message(color);
    message.destinationName = "house-perfect/rgb";
    client.send(message);
  }

  useEffect(() => {

    // set callback handlers
    client.onConnectionLost = onConnectionLost;
    client.onMessageArrived = onMessageArrived;

    // connect the client
    client.connect({onSuccess:onConnect, onFailure:() => {console.log("failure");}});


    // called when the client connects
    function onConnect() {
      // Once a connection has been made, make a subscription and send a message.
      console.log("onConnect");
      //metrics
      client.subscribe("house-perfect/humidity");
      client.subscribe("house-perfect/temperature");
      client.subscribe("house-perfect/detection");
      client.subscribe("house-perfect/gas");
      client.subscribe("house-perfect/fire");

      //pilot
      client.subscribe("house-perfect/rgb");
      client.subscribe("house-perfect/led");
      client.subscribe("house-perfect/bip");
    }

    // called when the client loses its connection
    function onConnectionLost(responseObject) {
      if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost:"+responseObject.errorMessage);
      }
    }

    // called when a message arrives
    function  onMessageArrived(message) {
      console.log("onMessageArrived:"+message.payloadString);
      console.log(message);
      switch(message.destinationName) {
        case 'house-perfect/led-external':
          toggleSwitchExternal();
          break;
        case 'house-perfect/humidity':
          setHumidity(parseInt(message.payloadString, 10));
          break;
        case 'house-perfect/temperature':
          setTemperature(parseInt(message.payloadString, 10));
          break;
        case 'house-perfect/detection':
          setDetection(message.payloadString);
          break;
        case 'house-perfect/porte':
          setPorte(message.payloadString);
          break;
        case 'house-perfect/gas':
          setGas(message.payloadString);
          break;
        case 'house-perfect/fire':
          setFire(message.payloadString);
          break;
      }
    }

  }, [])


  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <View style={styles.viewRow}>
        <View style={styles.viewSquare}>
          <Pressable style={styles.pressableCommon} onPress={() => {useRGB("blue")}}>
            <Text style={styles.pressableText}>Blue</Text>
          </Pressable>
          <Pressable style={styles.pressableSuccess} onPress={() => {useRGB("green")}}>
            <Text style={styles.pressableText}>Green</Text>
          </Pressable>
          <Pressable style={styles.pressableDanger} onPress={() => {useRGB("red")}}>
            <Text style={styles.pressableText}>Red</Text>
          </Pressable>
        </View>
        <View style={styles.viewSquare}>
          <Text>LED</Text>
          <Switch
            trackColor={{ false: "#767577", true: "#81b0ff" }}
            thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
            ios_backgroundColor="#3e3e3e"
            onValueChange={toggleSwitch}
            value={isEnabled}
          />
        </View>
      </View>
      <View style={styles.viewRow}>
        <View style={styles.viewSquare}>
          <Text>{`humidity : ${humidity} %`}</Text>
        </View>
        <View style={styles.viewSquare}>
          <Text>{`temperature : ${temperature} °`}</Text>
        </View>
      </View>
      <View style={styles.viewRow}>
        <View style={styles.viewSquare}>
          <Text>{`detection : ${detection}`}</Text>
        </View>
        <View style={styles.viewSquare}>
          <Text>{`porte : ${porte}`}</Text>
        </View>
      </View>
      <View style={styles.viewRow}>
        <View style={styles.viewSquare}>
          <Text>{`gaz : ${gas}`}</Text>
        </View>
        <View style={styles.viewSquare}>
          <Text>{`flamme : ${fire}`}</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  viewSquare: {
    width: "50%",
    height: "100%",
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  viewRow: {
    flexDirection: 'row',
    width: "100%",
    height: "25%",
  },
  container: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    height: "100%",
    width: "100%",
  },
  buttonGroup: {
    flexDirection: "row",
    zIndex: 0.5,
    width: "50%",
    justifyContent: "space-between",
  },
  pressableCommon: {
    zIndex: 1,
    padding: 5,
    backgroundColor: "lightblue",
  },
  pressableDanger: {
    padding: 5,
    backgroundColor: "red",
  },
  pressableSuccess: {
    padding: 5,
    backgroundColor: "green",
  },
  pressableText: {
    color: "white"
  },
  pressableLed: {
    padding: 5,
    backgroundColor: "dark",
  },
});
